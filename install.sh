#/bin/sh

set -euf

if [ "$EUID" -ne 0 ]
  then echo "Must be run as root";
  exit;
fi

chmod +x iptables
mkdir -v /etc/iptables
cp -v iptables /etc/init.d/
cp -v iptables.conf iptables.rules /etc/iptables/
sudo update-rc.d iptables defaults
