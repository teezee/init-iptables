# Iptables

Init script and persistent configuration and rules for iptables.

## Installation

To install run `install.sh`. 
        
## Usage

Provides the following files

- `/etc/init.d/iptables`: The init script to manage the iptables service
- `/etc/iptables/iptables.conf`: Config file
- `/etc/iptables/iptables.rules`: Firewall rules file

To start/stop the service or get information about effective rules, run

        sudo service iptables {start|stop|force-stop|restart|force-reload|status}

## Configuration

### rules

Iptables rules are configured in `/etc/iptables/rules`.
You *must* review that file and adjust to your needs.
If you do not know how to do that, first learn how to use iptables.
A good starting point might be [Ubuntu IptablesHowto][].

[Ubuntu IptablesHowto]: https://help.ubuntu.com/community/IptablesHowTo

By default `/etc/iptables/rules` only accepts incoming ssh traffic.

### iptables.conf

The config file `/etc/iptables/iptables.conf` allows configuration of iptables through variables,
that are evaluated by the iptables init script.

- `SAVE_NEW_RULES`: Save `1` or do not save `0` rules on service shutdown. Default `0`
- `MODULES`: A list of space separated values. Specify the modules to load, when the service starts.
- `ENABLE_ROUTING`: Enable `1` or disable packet forwarding for IPv4. Default `1`
- `ACCEPT_REDIRECTS`: Accept `1` or reject `0` ICMP redirects. Default `0`
- `ACCEPT_SOURCE_ROUTE`: Accept `1` or reject `0` IP source route packets. For routers.Default `0`
- `ICMP_ECHO_IGNORE_BROADCASTS`: Ignore `1` or allow `0` ICMP echo broadcasts. Beware ICMP echo floods. Default `1`
- `ICMP_IGNORE_BOGUS`: Ignore `1` or allow `0` bogus error message responses. Default `1`

Changes are not permanent, but are loaded every time iptables starts.
If you want to make the changes permanent, you have to edit `/etc/sysctl.conf` manually and then load configuration with

        sysctl -p


## References

- [iptables-init-debian](https://github.com/Sirtea/iptables-init-debian)
- [iptables-persistent](https://github.com/zertrin/iptables-persistent/blob/master/iptables-persistent)
- [DROP vs. REJECT](https://github.com/sebres/PoC/tree/master/FW.IDS-DROP-vs-REJECT)
